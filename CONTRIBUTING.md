# Contributing

You can add your grammars via merge requests (pull requests).

Grammar will have to pass the checks, and if checks are ok, binaries will be
available on remote storage (see README.md) for testing.

Please add only 1 grammar per merge request

Each grammar must:

* be in its own directory
* define a version in grammar yml file
* have a README.md explaining what this grammar does

If everything is fine, grammar will be merged and binaries pushed to storage
with their version, available to anyone for download.