#!/bin/bash

GITLABTOKEN=$1
CI_PROJECT_ID=$2
HERO=$3
CI_COMMIT_SHA=$4
CI_COMMIT_BEFORE_SHA=$5

CI_MERGE_REQUEST_IID=$6

set -e
set +x

if [ "a$GITLABTOKEN" == "a" ]; then
  echo "missing gitlab token"
  exit 1
fi
if [ "a$HERO" == "a" ]; then
  echo "missing hero token"
  exit 1
fi
if [ "a$CI_PROJECT_ID" == "a" ]; then
  echo "missing gitlab project id"
  exit 1
fi


if [ -e hero-file ]; then
    echo "hero-file present"
else
    wget -O hero-file https://github.com/osallou/herodote-file/releases/download/0.5.0/hero-file.linux.amd64
    chmod +x hero-file
fi

if [ -e yq ]; then
    echo "yq present"
else
    wget -O yq https://github.com/mikefarah/yq/releases/download/2.4.1/yq_linux_amd64
    chmod +x yq
fi
if [ -e logol2-client ]; then
    echo "logol2 client present"
else
    # Get latest logol2-client
    CLIENT=`curl https://gitlab.inria.fr/api/v4/projects/17706/repository/tags | jq -r [.[]][0].name`
    wget -O logol2-client https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2/logol2-client/$CLIENT/logol2-client-$CLIENT.linux.amd64
    chmod +x logol2-client
fi
# CI_COMMIT_BEFORE_SHA..CI_COMMIT_SHA
echo "Get diff between previous commit ${CI_COMMIT_BEFORE_SHA} and current commit ${CI_COMMIT_SHA}"
git diff --name-only ${CI_COMMIT_BEFORE_SHA}..${CI_COMMIT_SHA} | grep yml > diff.txt

input="diff.txt"
workdir=${PWD}
while IFS= read -r line
do
  cd $workdir
  if [ "$line" == ".gitlab-ci.yml" ]; then
      continue
  fi 
  echo "New/updated grammar: $line"
  grammarDir=$(dirname "$line")
  ./logol2-client --grammar $line --check
  ./logol2-client --grammar $line --output $grammarDir
  filename=$(basename -- "$line")
  extension="${filename##*.}"
  filename="${filename%.*}"
  echo "filename: $filename"
  cd $grammarDir && rm -f go.mod go.sum && go mod init $filename && go mod tidy  && \
      GOOS=linux GOARCH=amd64 go build -o $filename.linux.amd64 . && \
      GOOS=darwin GOARCH=amd64 go build -o $filename.darwin.amd64 . && \
      GOOS=windows GOARCH=amd64 go build  -o $filename.windows.amd64.exe .
  VERSION=`./$filename.linux.amd64 --grammar $filename.yml | ../yq r - version`
  TS=`date +%s`
  if [ "a$VERSION" == "a" ]; then
      VERSION="$TS-SNAPSHOT"
      echo "should upload to snapshot $filename-$VERSION" 
      ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.linux.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.linux.amd64
      ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.darwin.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.darwin.amd64
      ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.windows.amd64.exe --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.windows.amd64.exe
  else
      if [ "a$CI_MERGE_REQUEST_IID" == "a" ]; then
          echo "should upload to release $filename-$VERSION"
	  ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.linux.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.linux.amd64
	  ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.darwin.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.darwin.amd64
	  ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.windows.amd64.exe --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION/$filename-$VERSION.windows.amd64.exe
      else
          # should send a note on merge request for binary availability url
          # https://docs.gitlab.com/ee/api/notes.html#create-new-merge-request-note
          echo "merge request: should upload to snapshot $filename-$VERSION-$TS"
	  ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.linux.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION-SNAPSHOT/$filename-$VERSION-$TS.linux.amd64
	  ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.darwin.amd64 --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION-SNAPSHOT/$filename-$VERSION-$TS.darwin.amd64
         ../hero-file --os-auth-token "$HERO" --os-storage-url https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e upload osallou_artifacts $filename.windows.amd64.exe --object-name org.irisa.genouest.logol2.grammars/$filename/$VERSION-SNAPSHOT/$filename-$VERSION-$TS.windows.amd64.exe


	  ARTIFACTURL="https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2.grammars/$filename/$VERSION-SNAPSHOT/"
	  curl --header "PRIVATE-TOKEN: $GITLABTOKEN" --data "Grammar artifacts for $filename build are available at $ARTIFACTURL" --request POST https://gitlab.inria.fr/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes
      fi
  fi


done < "$input"
