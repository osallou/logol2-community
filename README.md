# logol2 community grammars

## grammars

Grammars release and snapshot files are available at [https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2.grammars/](https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2.grammars/).

Merge requests are saved in -SNAPSHOT version directories. Once merged, they will be moved to their version.
If grammar does not specify a version, it is saved with a timestamp as version in -SNAPSHOT version directory too.
